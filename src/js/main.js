;(function() {
  const headerSection = $('#headerSection');
  const galleyGrid = $('.gallery-grid');
  const accordion = $('.accordion');
  const photoGallery = '#photoGallery';
  const contentEditor = $('.content');

  let scrolled = $(window).scrollTop();

  window.onscroll = function() {
    scrolled = $(window).scrollTop();

    // handle nav menu position
    setNavPosition(scrolled, headerSection);
    setToTopArrow(scrolled);
  }

  if (galleyGrid.length) {
    $(window).on('load', function() {
      galleyGrid.masonry({
        percentPosition: true,
        itemSelector: '.gallery-grid__item',
      });
    })
  }

  if (accordion.length) {
    $('.accordion__title').click(function() {
      $(this).next('.accordion__content').slideToggle('400');
    })
  }

  if ($(photoGallery).length) {
    baguetteBox.run(photoGallery, {
      noScrollbars: true,
      ignoreClass: 'video',
      buttons: true,
    });
  }

  if (contentEditor.length) {
    console.log(contentEditor)
    contentEditor.each(function() {
      $(this).find('iframe[src*="youtube"]').each(function() {
        $(this).wrap('<div class="responsive-iframe"></div>');
      })
    })
  }

  // close modal on button click
  $(document).on('click', '.js-modal-close', function() {
    $(this).closest('.modal').removeClass('active');
  })

  // mobile nav
  $(document).on('click', '.js-toggle-mobile-nav', function(e) {
    e.preventDefault();
    $('.top-nav__menu').toggleClass('open');
  })

  // pay button
  $(document).on('click', '.pay-btn__triger', function(e) {
    e.preventDefault();
    $(this).closest('.pay-btn').find('.pay-btn__tost').toggleClass('active');
  })

  $(document).mouseup(function(e) {
    const toast = $('.pay-btn__tost');
    if (!toast.is(e.target) && !toast.has(e.target).length) {
      $('.pay-btn__tost').removeClass('active')
    }
  });
})();

function setNavPosition(scrolled, headerSection) {
  const headerHeight = headerSection.height();
  const nav = $('.top-nav');
  const toggledClass = 'top-nav_fixed';

  if (scrolled > headerHeight) {
    nav.addClass(toggledClass);
  } else {
    nav.removeClass(toggledClass);
  }
}

function onModalShow(modalId) {
  let $modal = $('#' + modalId);

  $modal.addClass('active');
};

function setToTopArrow(scrolled) {
  const btn = $('.to-top-button');

  if (scrolled > 1000) {
    btn.fadeIn(200);
  } else {
    btn.fadeOut(200);
  }
}

function scrollToTop() {
  $('html, body').animate({ scrollTop: 0 }, 700);
  return false;
}