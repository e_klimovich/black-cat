;(function() {
  const options = [
    {
      id: 1,
      question: 'Вот он первый вопрос подьехал. Че кого?',
      answers: [
        {
          key: 'А че?',
          value: '1',
        },
        {
          key: 'Да ниче',
          value: '2',
        },
        {
          key: 'Норм',
          value: '3',
        },
      ],
      correctAnswer: '1',
    },
    {
      id: 2,
      question: 'Летят два гуля один на север другой зеленый сколько лет ежику?',
      answers: [
        {
          key: 'Семь полных лет',
          value: '1',
        },
        {
          key: 'Три с половиной',
          value: '2',
        },
        {
          key: 'Сереневый',
          value: '3',
        },
      ],
      correctAnswer: '3',
    }
  ]
  const $form = $('#gameForm');
  const $preloadingContainer = $form.closest('.modal__container');
  const GAME_KEY = 'gameInfo';
  const AD_KEY = 'adshowed';
  let currentQuestion;
  let totalScore = 0;

  setTimeout(function() {
    if (!Boolean(localStorage.getItem(AD_KEY))) {
      // onModalShow('game');
      // localStorage.setItem(AD_KEY, true)
    }
  }, 5000)
 
  /**
   * Render game question with answers
   * @param {object} option 
   */
  function onQuestionRender(option) {
    const $answers = $form.find('.js-game-answers').html('');

    $form.find('.js-game-score').html(`${option.id} из ${options.length}`);
    $form.find('.js-game-question').html(option.question);

    option.answers.forEach(function(item) {
      $answers.append(`<label><input type="radio" name="answer" value="${item.value}">${item.key}</label>`);
    });

    currentQuestion = option;
  }

  function onFinalGameRender() {
    const gameInfo = JSON.parse(
      localStorage.getItem(GAME_KEY)
    );

    if (gameInfo.isComplete) {
      $form.html(`<div class="game-form__desc">Вы прошли тест с ${gameInfo.totalScore} правильными ответами и полуичли 
      скидку ${gameInfo.totalScore}% на первый месяц обучения. Отправьте заявку для подтверждения скидки</div>
      <div class="form-group">
        <input type="text" name="name" placeholder="Имя" />
      </div>
      <div class="form-group">
        <input type="text" name="phone" placeholder="Телефон" />
      </div>
      <div class="form-group">
        <input type="text" name="discount" readonly value="${gameInfo.totalScore}%" />
      </div>
      <button type="button" class="btn">Отправить</button>
      `);
    }
  }

  /**
   * On first question render
   */
  if (localStorage.getItem(GAME_KEY) === null) {
    onQuestionRender(options[0]);
  } else {
    onFinalGameRender();
  }

  /**
   * On submit answer handler
  */
  $form.submit(function(e) {
    e.preventDefault();

    $preloadingContainer.addClass('preloading');

    const answer = $(this).serialize().split('=')[1];
    const isCoorectAnswer = currentQuestion.correctAnswer === answer;

    setTimeout(function() {
      $preloadingContainer.removeClass('preloading');

      if (isCoorectAnswer) {
        totalScore++;
      }

      if (options.length !== currentQuestion.id) {
        onQuestionRender(options[currentQuestion.id]);
      } else {
        const gameInfo = JSON.stringify({ isComplete: true, totalScore });
        localStorage.setItem(GAME_KEY, gameInfo);
        onFinalGameRender();
      }
    }, 1000)
  })
})();

